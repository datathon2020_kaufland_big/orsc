﻿/* Showing results for datathon_masterdata.xlsx */

/* CREATE TABLE */
CREATE TABLE datathon_masterdata(
item_id INT,
item_name VARCHAR(100),
unit VARCHAR(100),
order_qty INT,
transport_qty INT,
min_stock INT,
max_stock INT,
item_prio_ DECIMAL(4,3),
storage_cost DECIMAL(7,6),
mhd INT
);

/* INSERT QUERY NO: 1 */
INSERT INTO datathon_masterdata(item_id, item_name, unit, order_qty, transport_qty, min_stock, max_stock, item_prio_, storage_cost, mhd)
VALUES
(
101900, 'Dried Basil', 'ST', 20, 2880, 10, 40, 0, 0.000563, 548
);

/* INSERT QUERY NO: 2 */
INSERT INTO datathon_masterdata(item_id, item_name, unit, order_qty, transport_qty, min_stock, max_stock, item_prio_, storage_cost, mhd)
VALUES
(
181130, 'Softcookies', 'ST', 28, 1680, 30, 150, 0.002, 0.000568, 98
);

/* INSERT QUERY NO: 3 */
INSERT INTO datathon_masterdata(item_id, item_name, unit, order_qty, transport_qty, min_stock, max_stock, item_prio_, storage_cost, mhd)
VALUES
(
188640, 'Carrots juice', 'ST', 12, 1296, 6, 60, 0.002, 0.000473, 243
);

/* INSERT QUERY NO: 4 */
INSERT INTO datathon_masterdata(item_id, item_name, unit, order_qty, transport_qty, min_stock, max_stock, item_prio_, storage_cost, mhd)
VALUES
(
192100, 'Ice Cream stick', 'ST', 24, 5304, 40, 100, 0.002, 0.000358, 480
);

/* INSERT QUERY NO: 5 */
INSERT INTO datathon_masterdata(item_id, item_name, unit, order_qty, transport_qty, min_stock, max_stock, item_prio_, storage_cost, mhd)
VALUES
(
211110, 'Cake Cream', 'ST', 48, 5760, 12, 48, 0.002, 0.000557, 243
);

/* INSERT QUERY NO: 6 */
INSERT INTO datathon_masterdata(item_id, item_name, unit, order_qty, transport_qty, min_stock, max_stock, item_prio_, storage_cost, mhd)
VALUES
(
236490, 'Chiken spice', 'ST', 30, 6300, 10, 40, 0.002, 0.000584, 405
);

/* INSERT QUERY NO: 7 */
INSERT INTO datathon_masterdata(item_id, item_name, unit, order_qty, transport_qty, min_stock, max_stock, item_prio_, storage_cost, mhd)
VALUES
(
250520, 'Corn flour', 'ST', 12, 1296, 12, 60, 0.003, 0.000552, 547
);

/* INSERT QUERY NO: 8 */
INSERT INTO datathon_masterdata(item_id, item_name, unit, order_qty, transport_qty, min_stock, max_stock, item_prio_, storage_cost, mhd)
VALUES
(
263350, 'Deo', 'ST', 12, 3072, 18, 120, 0.004, 0.000944, 821
);

/* INSERT QUERY NO: 9 */
INSERT INTO datathon_masterdata(item_id, item_name, unit, order_qty, transport_qty, min_stock, max_stock, item_prio_, storage_cost, mhd)
VALUES
(
280560, 'Soda', 'ST', 6, 264, 6, 60, 0.005, 0.000561, 135
);

/* INSERT QUERY NO: 10 */
INSERT INTO datathon_masterdata(item_id, item_name, unit, order_qty, transport_qty, min_stock, max_stock, item_prio_, storage_cost, mhd)
VALUES
(
293220, 'Bio Bar', 'ST', 360, 19440, 10, 50, 0.005, 0.000758, 248
);

/* INSERT QUERY NO: 11 */
INSERT INTO datathon_masterdata(item_id, item_name, unit, order_qty, transport_qty, min_stock, max_stock, item_prio_, storage_cost, mhd)
VALUES
(
302920, 'Mayo', 'ST', 8, 1600, 16, 90, 0.005, 0.000883, 135
);

/* INSERT QUERY NO: 12 */
INSERT INTO datathon_masterdata(item_id, item_name, unit, order_qty, transport_qty, min_stock, max_stock, item_prio_, storage_cost, mhd)
VALUES
(
334060, 'Bio gum', 'ST', 500, 24500, 100, 600, 0.005, 0.000644, 821
);

/* INSERT QUERY NO: 13 */
INSERT INTO datathon_masterdata(item_id, item_name, unit, order_qty, transport_qty, min_stock, max_stock, item_prio_, storage_cost, mhd)
VALUES
(
346200, 'Snacks', 'ST', 24, 1680, 10, 100, 0.006, 0.000926, 203
);

/* INSERT QUERY NO: 14 */
INSERT INTO datathon_masterdata(item_id, item_name, unit, order_qty, transport_qty, min_stock, max_stock, item_prio_, storage_cost, mhd)
VALUES
(
373470, 'Ayran', 'ST', 12, 1140, 12, 84, 0.006, 0.000412, 15
);

/* INSERT QUERY NO: 15 */
INSERT INTO datathon_masterdata(item_id, item_name, unit, order_qty, transport_qty, min_stock, max_stock, item_prio_, storage_cost, mhd)
VALUES
(
381840, 'Jelly Candy', 'ST', 18, 1944, 18, 72, 0.008, 0.000741, 135
);

/* INSERT QUERY NO: 16 */
INSERT INTO datathon_masterdata(item_id, item_name, unit, order_qty, transport_qty, min_stock, max_stock, item_prio_, storage_cost, mhd)
VALUES
(
387440, 'Milk', 'ST', 20, 960, 20, 120, 0.009, 0.000554, 10
);

/* INSERT QUERY NO: 17 */
INSERT INTO datathon_masterdata(item_id, item_name, unit, order_qty, transport_qty, min_stock, max_stock, item_prio_, storage_cost, mhd)
VALUES
(
400830, 'Tea', 'ST', 12, 2400, 6, 30, 0.009, 0.0012, 821
);

/* INSERT QUERY NO: 18 */
INSERT INTO datathon_masterdata(item_id, item_name, unit, order_qty, transport_qty, min_stock, max_stock, item_prio_, storage_cost, mhd)
VALUES
(
410980, 'Baby shampoo', 'ST', 21, 1260, 10, 40, 0.009, 0.001219, 821
);

/* INSERT QUERY NO: 19 */
INSERT INTO datathon_masterdata(item_id, item_name, unit, order_qty, transport_qty, min_stock, max_stock, item_prio_, storage_cost, mhd)
VALUES
(
436620, 'Handkerchief', 'ST', 20, 2160, 20, 140, 0.009, 0.001221, 0
);

/* INSERT QUERY NO: 20 */
INSERT INTO datathon_masterdata(item_id, item_name, unit, order_qty, transport_qty, min_stock, max_stock, item_prio_, storage_cost, mhd)
VALUES
(
470570, 'Pickled Mushrooms Whole', 'ST', 12, 1728, 6, 30, 0.01, 0.001141, 435
);

/* INSERT QUERY NO: 21 */
INSERT INTO datathon_masterdata(item_id, item_name, unit, order_qty, transport_qty, min_stock, max_stock, item_prio_, storage_cost, mhd)
VALUES
(
475500, 'Body cream', 'ST', 8, 1440, 6, 48, 0.01, 0.001427, 0
);

/* INSERT QUERY NO: 22 */
INSERT INTO datathon_masterdata(item_id, item_name, unit, order_qty, transport_qty, min_stock, max_stock, item_prio_, storage_cost, mhd)
VALUES
(
478170, 'Humus', 'ST', 8, 1280, 6, 24, 0.01, 0.00106, 29
);

/* INSERT QUERY NO: 23 */
INSERT INTO datathon_masterdata(item_id, item_name, unit, order_qty, transport_qty, min_stock, max_stock, item_prio_, storage_cost, mhd)
VALUES
(
489130, 'Dried Herbs', 'ST', 6, 180, 6, 24, 0.011, 0.001282, 0
);

/* INSERT QUERY NO: 24 */
INSERT INTO datathon_masterdata(item_id, item_name, unit, order_qty, transport_qty, min_stock, max_stock, item_prio_, storage_cost, mhd)
VALUES
(
500970, 'Cold tea', 'ST', 12, 1512, 6, 48, 0.011, 0.000606, 338
);

/* INSERT QUERY NO: 25 */
INSERT INTO datathon_masterdata(item_id, item_name, unit, order_qty, transport_qty, min_stock, max_stock, item_prio_, storage_cost, mhd)
VALUES
(
514610, 'Rice', 'ST', 12, 720, 6, 48, 0.011, 0.001374, 405
);

/* INSERT QUERY NO: 26 */
INSERT INTO datathon_masterdata(item_id, item_name, unit, order_qty, transport_qty, min_stock, max_stock, item_prio_, storage_cost, mhd)
VALUES
(
520170, 'Pickled Peppers', 'ST', 12, 768, 6, 30, 0.012, 0.001414, 289
);

/* INSERT QUERY NO: 27 */
INSERT INTO datathon_masterdata(item_id, item_name, unit, order_qty, transport_qty, min_stock, max_stock, item_prio_, storage_cost, mhd)
VALUES
(
529330, 'Ice Cream', 'ST', 40, 5760, 20, 100, 0.012, 0.000458, 405
);

/* INSERT QUERY NO: 28 */
INSERT INTO datathon_masterdata(item_id, item_name, unit, order_qty, transport_qty, min_stock, max_stock, item_prio_, storage_cost, mhd)
VALUES
(
559090, 'Pickles', 'ST', 6, 864, 6, 60, 0.012, 0.001051, 360
);

/* INSERT QUERY NO: 29 */
INSERT INTO datathon_masterdata(item_id, item_name, unit, order_qty, transport_qty, min_stock, max_stock, item_prio_, storage_cost, mhd)
VALUES
(
559450, 'Candy', 'ST', 64, 5120, 32, 160, 0.012, 0.000929, 141
);

/* INSERT QUERY NO: 30 */
INSERT INTO datathon_masterdata(item_id, item_name, unit, order_qty, transport_qty, min_stock, max_stock, item_prio_, storage_cost, mhd)
VALUES
(
560010, 'Muesli', 'ST', 20, 1200, 10, 60, 0.012, 0.001566, 135
);

/* INSERT QUERY NO: 31 */
INSERT INTO datathon_masterdata(item_id, item_name, unit, order_qty, transport_qty, min_stock, max_stock, item_prio_, storage_cost, mhd)
VALUES
(
567280, 'Creme sensitive', 'ST', 12, 2016, 6, 24, 0.012, 0.001477, 540
);

/* INSERT QUERY NO: 32 */
INSERT INTO datathon_masterdata(item_id, item_name, unit, order_qty, transport_qty, min_stock, max_stock, item_prio_, storage_cost, mhd)
VALUES
(
576340, 'Tooth brush', 'ST', 12, 3360, 6, 42, 0.012, 0.001781, 0
);

/* INSERT QUERY NO: 33 */
INSERT INTO datathon_masterdata(item_id, item_name, unit, order_qty, transport_qty, min_stock, max_stock, item_prio_, storage_cost, mhd)
VALUES
(
598220, 'Freezed veggetables', 'ST', 25, 1575, 10, 40, 0.013, 0.001141, 274
);

/* INSERT QUERY NO: 34 */
INSERT INTO datathon_masterdata(item_id, item_name, unit, order_qty, transport_qty, min_stock, max_stock, item_prio_, storage_cost, mhd)
VALUES
(
609130, 'Protein Bar', 'ST', 12, 3600, 6, 48, 0.014, 0.001361, 225
);

/* INSERT QUERY NO: 35 */
INSERT INTO datathon_masterdata(item_id, item_name, unit, order_qty, transport_qty, min_stock, max_stock, item_prio_, storage_cost, mhd)
VALUES
(
611060, 'Frozen Baggette', 'ST', 7, 1176, 14, 56, 0.014, 0.000995, 141
);

/* INSERT QUERY NO: 36 */
INSERT INTO datathon_masterdata(item_id, item_name, unit, order_qty, transport_qty, min_stock, max_stock, item_prio_, storage_cost, mhd)
VALUES
(
619320, 'Shampoo', 'ST', 12, 1152, 6, 48, 0.015, 0.001279, 0
);

/* INSERT QUERY NO: 37 */
INSERT INTO datathon_masterdata(item_id, item_name, unit, order_qty, transport_qty, min_stock, max_stock, item_prio_, storage_cost, mhd)
VALUES
(
624160, 'Salami', 'ST', 30, 2520, 6, 30, 0.015, 0.001267, 68
);

/* INSERT QUERY NO: 38 */
INSERT INTO datathon_masterdata(item_id, item_name, unit, order_qty, transport_qty, min_stock, max_stock, item_prio_, storage_cost, mhd)
VALUES
(
636440, 'Matches', 'ST', 288, 8640, 360, 3600, 0.015, 0.002091, 999
);

/* INSERT QUERY NO: 39 */
INSERT INTO datathon_masterdata(item_id, item_name, unit, order_qty, transport_qty, min_stock, max_stock, item_prio_, storage_cost, mhd)
VALUES
(
649970, 'Mashed potatoes', 'ST', 14, 504, 6, 42, 0.016, 0.001306, 274
);

/* INSERT QUERY NO: 40 */
INSERT INTO datathon_masterdata(item_id, item_name, unit, order_qty, transport_qty, min_stock, max_stock, item_prio_, storage_cost, mhd)
VALUES
(
653750, 'Black Tea', 'ST', 6, 1800, 12, 60, 0.016, 0.001905, 822
);

/* INSERT QUERY NO: 41 */
INSERT INTO datathon_masterdata(item_id, item_name, unit, order_qty, transport_qty, min_stock, max_stock, item_prio_, storage_cost, mhd)
VALUES
(
657690, 'Cleaning liquid', 'ST', 12, 480, 6, 36, 0.017, 0.000406, 675
);

/* INSERT QUERY NO: 42 */
INSERT INTO datathon_masterdata(item_id, item_name, unit, order_qty, transport_qty, min_stock, max_stock, item_prio_, storage_cost, mhd)
VALUES
(
658850, 'Pickled Mushrooms', 'ST', 12, 1440, 6, 30, 0.017, 0.00173, 540
);

/* INSERT QUERY NO: 43 */
INSERT INTO datathon_masterdata(item_id, item_name, unit, order_qty, transport_qty, min_stock, max_stock, item_prio_, storage_cost, mhd)
VALUES
(
662410, 'Sweetener', 'ST', 80, 62400, 80, 160, 0.017, 0.000091, 798
);

/* INSERT QUERY NO: 44 */
INSERT INTO datathon_masterdata(item_id, item_name, unit, order_qty, transport_qty, min_stock, max_stock, item_prio_, storage_cost, mhd)
VALUES
(
666050, 'Washing liquid', 'ST', 20, 800, 5, 50, 0.017, 0.00166, 1096
);

/* INSERT QUERY NO: 45 */
INSERT INTO datathon_masterdata(item_id, item_name, unit, order_qty, transport_qty, min_stock, max_stock, item_prio_, storage_cost, mhd)
VALUES
(
680220, 'Washing liquid 750ml', 'ST', 10, 480, 10, 60, 0.017, 0.00137, 1095
);

/* INSERT QUERY NO: 46 */
INSERT INTO datathon_masterdata(item_id, item_name, unit, order_qty, transport_qty, min_stock, max_stock, item_prio_, storage_cost, mhd)
VALUES
(
684750, 'Black Tea w', 'ST', 6, 2184, 12, 72, 0.017, 0.001905, 821
);

/* INSERT QUERY NO: 47 */
INSERT INTO datathon_masterdata(item_id, item_name, unit, order_qty, transport_qty, min_stock, max_stock, item_prio_, storage_cost, mhd)
VALUES
(
688650, 'Razor blade', 'ST', 40, 2560, 10, 50, 0.017, 0.001236, 0
);

/* INSERT QUERY NO: 48 */
INSERT INTO datathon_masterdata(item_id, item_name, unit, order_qty, transport_qty, min_stock, max_stock, item_prio_, storage_cost, mhd)
VALUES
(
690940, 'Deospray', 'ST', 6, 2304, 6, 36, 0.017, 0.001886, 0
);

/* INSERT QUERY NO: 49 */
INSERT INTO datathon_masterdata(item_id, item_name, unit, order_qty, transport_qty, min_stock, max_stock, item_prio_, storage_cost, mhd)
VALUES
(
723160, 'Dog Food', 'ST', 6, 924, 6, 24, 0.018, 0.001332, 487
);

/* INSERT QUERY NO: 50 */
INSERT INTO datathon_masterdata(item_id, item_name, unit, order_qty, transport_qty, min_stock, max_stock, item_prio_, storage_cost, mhd)
VALUES
(
735390, 'Cooking Knife', 'ST', 96, 3072, 12, 48, 0.019, 0.002562, 0
);

/* INSERT QUERY NO: 51 */
INSERT INTO datathon_masterdata(item_id, item_name, unit, order_qty, transport_qty, min_stock, max_stock, item_prio_, storage_cost, mhd)
VALUES
(
769020, 'Light Beer', 'ST', 24, 1728, 12, 60, 0.021, 0.000898, 240
);

/* INSERT QUERY NO: 52 */
INSERT INTO datathon_masterdata(item_id, item_name, unit, order_qty, transport_qty, min_stock, max_stock, item_prio_, storage_cost, mhd)
VALUES
(
769590, 'Coffee grains', 'ST', 12, 2376, 6, 72, 0.021, 0.002062, 387
);

/* INSERT QUERY NO: 53 */
INSERT INTO datathon_masterdata(item_id, item_name, unit, order_qty, transport_qty, min_stock, max_stock, item_prio_, storage_cost, mhd)
VALUES
(
772860, 'Sweetened Milk', 'ST', 12, 1848, 12, 24, 0.022, 0.001637, 135
);

/* INSERT QUERY NO: 54 */
INSERT INTO datathon_masterdata(item_id, item_name, unit, order_qty, transport_qty, min_stock, max_stock, item_prio_, storage_cost, mhd)
VALUES
(
776100, 'Pale Ale Beer', 'ST', 20, 800, 10, 60, 0.023, 0.001201, 105
);

/* INSERT QUERY NO: 55 */
INSERT INTO datathon_masterdata(item_id, item_name, unit, order_qty, transport_qty, min_stock, max_stock, item_prio_, storage_cost, mhd)
VALUES
(
785550, 'Red cheese', 'ST', 20, 3840, 10, 30, 0.023, 0.001609, 63
);

/* INSERT QUERY NO: 56 */
INSERT INTO datathon_masterdata(item_id, item_name, unit, order_qty, transport_qty, min_stock, max_stock, item_prio_, storage_cost, mhd)
VALUES
(
786520, 'Choko Ice Cream', 'ST', 9, 540, 40, 100, 0.024, 0.002173, 237
);

/* INSERT QUERY NO: 57 */
INSERT INTO datathon_masterdata(item_id, item_name, unit, order_qty, transport_qty, min_stock, max_stock, item_prio_, storage_cost, mhd)
VALUES
(
789110, 'AirFreshner', 'ST', 10, 1440, 5, 40, 0.025, 0.002764, 730
);

/* INSERT QUERY NO: 58 */
INSERT INTO datathon_masterdata(item_id, item_name, unit, order_qty, transport_qty, min_stock, max_stock, item_prio_, storage_cost, mhd)
VALUES
(
1000500, 'Washing powder', 'ST', 6, 252, 6, 48, 0.026, 0.002655, 548
);

/* INSERT QUERY NO: 59 */
INSERT INTO datathon_masterdata(item_id, item_name, unit, order_qty, transport_qty, min_stock, max_stock, item_prio_, storage_cost, mhd)
VALUES
(
4017510, 'Soy drink', 'ST', 8, 760, 12, 24, 0.026, 0.002767, 203
);

/* INSERT QUERY NO: 60 */
INSERT INTO datathon_masterdata(item_id, item_name, unit, order_qty, transport_qty, min_stock, max_stock, item_prio_, storage_cost, mhd)
VALUES
(
4018520, 'Frozen Burger', 'ST', 12, 756, 12, 72, 0.027, 0.001668, 21
);

/* INSERT QUERY NO: 61 */
INSERT INTO datathon_masterdata(item_id, item_name, unit, order_qty, transport_qty, min_stock, max_stock, item_prio_, storage_cost, mhd)
VALUES
(
5004430, 'Frozen Pizza', 'ST', 12, 840, 12, 72, 0.028, 0.002826, 165
);

/* INSERT QUERY NO: 62 */
INSERT INTO datathon_masterdata(item_id, item_name, unit, order_qty, transport_qty, min_stock, max_stock, item_prio_, storage_cost, mhd)
VALUES
(
5016300, 'Cooking creeme', 'ST', 12, 720, 12, 36, 0.029, 0.002382, 113
);

/* INSERT QUERY NO: 63 */
INSERT INTO datathon_masterdata(item_id, item_name, unit, order_qty, transport_qty, min_stock, max_stock, item_prio_, storage_cost, mhd)
VALUES
(
8017950, 'Cheese role', 'KG ', 8, 1304, 2, 10, 0.029, 0.00652, 23
);

/* INSERT QUERY NO: 64 */
INSERT INTO datathon_masterdata(item_id, item_name, unit, order_qty, transport_qty, min_stock, max_stock, item_prio_, storage_cost, mhd)
VALUES
(
9011100, 'Snack', 'ST', 16, 576, 6, 48, 0.034, 0.00275, 270
);

/* INSERT QUERY NO: 65 */
INSERT INTO datathon_masterdata(item_id, item_name, unit, order_qty, transport_qty, min_stock, max_stock, item_prio_, storage_cost, mhd)
VALUES
(
10012900, 'Towel', 'ST', 18, 792, 6, 24, 0.035, 0.002096, 0
);

/* INSERT QUERY NO: 66 */
INSERT INTO datathon_masterdata(item_id, item_name, unit, order_qty, transport_qty, min_stock, max_stock, item_prio_, storage_cost, mhd)
VALUES
(
10038910, 'WC Freshener', 'ST', 10, 2200, 12, 60, 0.036, 0.001241, 975
);

/* INSERT QUERY NO: 67 */
INSERT INTO datathon_masterdata(item_id, item_name, unit, order_qty, transport_qty, min_stock, max_stock, item_prio_, storage_cost, mhd)
VALUES
(
10042840, 'Juice', 'ST', 4, 144, 6, 36, 0.037, 0.003804, 274
);

/* INSERT QUERY NO: 68 */
INSERT INTO datathon_masterdata(item_id, item_name, unit, order_qty, transport_qty, min_stock, max_stock, item_prio_, storage_cost, mhd)
VALUES
(
10045810, 'Bio Juice', 'ST', 8, 800, 32, 192, 0.037, 0.002767, 158
);

/* INSERT QUERY NO: 69 */
INSERT INTO datathon_masterdata(item_id, item_name, unit, order_qty, transport_qty, min_stock, max_stock, item_prio_, storage_cost, mhd)
VALUES
(
12001300, 'White Wine', 'ST', 6, 288, 6, 36, 0.037, 0.001888, 365
);

/* INSERT QUERY NO: 70 */
INSERT INTO datathon_masterdata(item_id, item_name, unit, order_qty, transport_qty, min_stock, max_stock, item_prio_, storage_cost, mhd)
VALUES
(
12003400, 'Shaving creme', 'ST', 6, 2352, 6, 18, 0.039, 0.003229, 0
);

/* INSERT QUERY NO: 71 */
INSERT INTO datathon_masterdata(item_id, item_name, unit, order_qty, transport_qty, min_stock, max_stock, item_prio_, storage_cost, mhd)
VALUES
(
13002710, 'Cheese', 'ST', 16, 1248, 20, 100, 0.042, 0.002963, 405
);

/* INSERT QUERY NO: 72 */
INSERT INTO datathon_masterdata(item_id, item_name, unit, order_qty, transport_qty, min_stock, max_stock, item_prio_, storage_cost, mhd)
VALUES
(
17003310, 'Toilet paper', 'ST', 1, 100, 5, 80, 0.048, 0.004668, 0
);

/* INSERT QUERY NO: 73 */
INSERT INTO datathon_masterdata(item_id, item_name, unit, order_qty, transport_qty, min_stock, max_stock, item_prio_, storage_cost, mhd)
VALUES
(
17016900, 'Hot Salami', 'KG ', 32, 3168, 3, 15, 0.05, 0.015423, 45
);

/* INSERT QUERY NO: 74 */
INSERT INTO datathon_masterdata(item_id, item_name, unit, order_qty, transport_qty, min_stock, max_stock, item_prio_, storage_cost, mhd)
VALUES
(
18001910, 'Caramel candy', 'ST', 50, 3150, 20, 120, 0.051, 0.000423, 405
);

/* INSERT QUERY NO: 75 */
INSERT INTO datathon_masterdata(item_id, item_name, unit, order_qty, transport_qty, min_stock, max_stock, item_prio_, storage_cost, mhd)
VALUES
(
23012800, 'Gums', 'ST', 600, 43200, 100, 600, 0.051, 0.000468, 333
);

/* INSERT QUERY NO: 76 */
INSERT INTO datathon_masterdata(item_id, item_name, unit, order_qty, transport_qty, min_stock, max_stock, item_prio_, storage_cost, mhd)
VALUES
(
23015350, 'Cleaning gel', 'ST', 12, 324, 10, 80, 0.054, 0.00366, 487
);

/* INSERT QUERY NO: 77 */
INSERT INTO datathon_masterdata(item_id, item_name, unit, order_qty, transport_qty, min_stock, max_stock, item_prio_, storage_cost, mhd)
VALUES
(
24003430, 'Frozen Pizza Pepperoni', 'ST', 5, 630, 10, 100, 0.055, 0.002261, 151
);

/* INSERT QUERY NO: 78 */
INSERT INTO datathon_masterdata(item_id, item_name, unit, order_qty, transport_qty, min_stock, max_stock, item_prio_, storage_cost, mhd)
VALUES
(
24013050, 'Washing gel', 'ST', 5, 240, 10, 80, 0.059, 0.004748, 365
);

/* INSERT QUERY NO: 79 */
INSERT INTO datathon_masterdata(item_id, item_name, unit, order_qty, transport_qty, min_stock, max_stock, item_prio_, storage_cost, mhd)
VALUES
(
24025850, 'Donut', 'ST', 48, 4224, 18, 90, 0.066, 0.000372, 180
);

/* INSERT QUERY NO: 80 */
INSERT INTO datathon_masterdata(item_id, item_name, unit, order_qty, transport_qty, min_stock, max_stock, item_prio_, storage_cost, mhd)
VALUES
(
24027460, 'Brandy', 'ST', 6, 264, 4, 24, 0.081, 0.006387, 0
);

/* INSERT QUERY NO: 81 */
INSERT INTO datathon_masterdata(item_id, item_name, unit, order_qty, transport_qty, min_stock, max_stock, item_prio_, storage_cost, mhd)
VALUES
(
27000200, 'Packed Meat', 'ST', 10, 520, 10, 30, 0.091, 0.003527, 7
);

/* INSERT QUERY NO: 82 */
INSERT INTO datathon_masterdata(item_id, item_name, unit, order_qty, transport_qty, min_stock, max_stock, item_prio_, storage_cost, mhd)
VALUES
(
27007910, 'Muskat', 'ST', 4, 176, 6, 36, 0.104, 0.005879, 180
);

/* INSERT QUERY NO: 83 */
INSERT INTO datathon_masterdata(item_id, item_name, unit, order_qty, transport_qty, min_stock, max_stock, item_prio_, storage_cost, mhd)
VALUES
(
27017360, 'Frozen Shrimps', 'ST', 10, 540, 10, 50, 0.107, 0.006653, 365
);

/* INSERT QUERY NO: 84 */
INSERT INTO datathon_masterdata(item_id, item_name, unit, order_qty, transport_qty, min_stock, max_stock, item_prio_, storage_cost, mhd)
VALUES
(
37000220, 'Mushrooms 250g', 'ST', 12, 360, 12, 120, 0.109, 0.00091, 1
);

/* INSERT QUERY NO: 85 */
INSERT INTO datathon_masterdata(item_id, item_name, unit, order_qty, transport_qty, min_stock, max_stock, item_prio_, storage_cost, mhd)
VALUES
(
38005020, 'Cat Food', 'ST', 20, 1760, 80, 400, 0.11, 0.00046, 487
);

/* INSERT QUERY NO: 86 */
INSERT INTO datathon_masterdata(item_id, item_name, unit, order_qty, transport_qty, min_stock, max_stock, item_prio_, storage_cost, mhd)
VALUES
(
39009280, 'Olives', 'KG ', 5, 4125, 5, 10, 0.114, 0.00275, 730
);

/* INSERT QUERY NO: 87 */
INSERT INTO datathon_masterdata(item_id, item_name, unit, order_qty, transport_qty, min_stock, max_stock, item_prio_, storage_cost, mhd)
VALUES
(
40001260, 'Chopped meat', 'ST', 22, 1408, 4, 44, 0.117, 0.000812, 5
);

/* INSERT QUERY NO: 88 */
INSERT INTO datathon_masterdata(item_id, item_name, unit, order_qty, transport_qty, min_stock, max_stock, item_prio_, storage_cost, mhd)
VALUES
(
40012940, 'White frozen bread', 'ST', 80, 1600, 120, 480, 0.133, 0.000154, 274
);

/* INSERT QUERY NO: 89 */
INSERT INTO datathon_masterdata(item_id, item_name, unit, order_qty, transport_qty, min_stock, max_stock, item_prio_, storage_cost, mhd)
VALUES
(
46008230, 'Instant Coffee', 'ST', 6, 570, 20, 100, 0.138, 0.00695, 548
);

/* INSERT QUERY NO: 90 */
INSERT INTO datathon_masterdata(item_id, item_name, unit, order_qty, transport_qty, min_stock, max_stock, item_prio_, storage_cost, mhd)
VALUES
(
47017660, 'Coffee 100g', 'ST', 20, 4500, 20, 100, 0.154, 0.000826, 412
);

/* INSERT QUERY NO: 91 */
INSERT INTO datathon_masterdata(item_id, item_name, unit, order_qty, transport_qty, min_stock, max_stock, item_prio_, storage_cost, mhd)
VALUES
(
47036010, 'Water 0.5L', 'ST', 12, 1368, 24, 160, 0.172, 0.000321, 270
);

/* INSERT QUERY NO: 92 */
INSERT INTO datathon_masterdata(item_id, item_name, unit, order_qty, transport_qty, min_stock, max_stock, item_prio_, storage_cost, mhd)
VALUES
(
47055000, 'Mint & Lemon', 'ST', 6, 504, 6, 24, 0.186, 0.003078, 0
);

/* INSERT QUERY NO: 93 */
INSERT INTO datathon_masterdata(item_id, item_name, unit, order_qty, transport_qty, min_stock, max_stock, item_prio_, storage_cost, mhd)
VALUES
(
48005040, 'Flour', 'ST', 10, 800, 5, 100, 0.204, 0.000364, 225
);

/* INSERT QUERY NO: 94 */
INSERT INTO datathon_masterdata(item_id, item_name, unit, order_qty, transport_qty, min_stock, max_stock, item_prio_, storage_cost, mhd)
VALUES
(
48005700, 'Merlot', 'ST', 12, 480, 12, 48, 0.236, 0.002184, 365
);

/* INSERT QUERY NO: 95 */
INSERT INTO datathon_masterdata(item_id, item_name, unit, order_qty, transport_qty, min_stock, max_stock, item_prio_, storage_cost, mhd)
VALUES
(
48040400, 'Ham', 'KG ', 7, 364, 3, 15, 0.258, 0.015205, 45
);

/* INSERT QUERY NO: 96 */
INSERT INTO datathon_masterdata(item_id, item_name, unit, order_qty, transport_qty, min_stock, max_stock, item_prio_, storage_cost, mhd)
VALUES
(
49027820, 'Filo Pastry 400g', 'ST', 24, 1050, 20, 100, 0.301, 0.000502, 20
);

/* INSERT QUERY NO: 97 */
INSERT INTO datathon_masterdata(item_id, item_name, unit, order_qty, transport_qty, min_stock, max_stock, item_prio_, storage_cost, mhd)
VALUES
(
50019350, 'Water 11L', 'ST', 1, 72, 10, 30, 0.512, 0.000855, 274
);

/* INSERT QUERY NO: 98 */
INSERT INTO datathon_masterdata(item_id, item_name, unit, order_qty, transport_qty, min_stock, max_stock, item_prio_, storage_cost, mhd)
VALUES
(
51001920, 'White Cheese', 'KG ', 8, 480, 4, 16, 0.558, 0.003807, 180
);

/* INSERT QUERY NO: 99 */
INSERT INTO datathon_masterdata(item_id, item_name, unit, order_qty, transport_qty, min_stock, max_stock, item_prio_, storage_cost, mhd)
VALUES
(
51014930, 'Filo Pastry 500g', 'ST', 20, 840, 20, 100, 0.596, 0.000902, 100
);

/* INSERT QUERY NO: 100 */
INSERT INTO datathon_masterdata(item_id, item_name, unit, order_qty, transport_qty, min_stock, max_stock, item_prio_, storage_cost, mhd)
VALUES
(
51021730, 'Oil', 'ST', 12, 720, 60, 720, 0.791, 0.000933, 274
);

/* INSERT QUERY NO: 101 */
INSERT INTO datathon_masterdata(item_id, item_name, unit, order_qty, transport_qty, min_stock, max_stock, item_prio_, storage_cost, mhd)
VALUES
(
63024630, 'Coffee', 'ST', 18, 720, 20, 100, 0.85, 0.001472, 338
);

/* INSERT QUERY NO: 102 */
INSERT INTO datathon_masterdata(item_id, item_name, unit, order_qty, transport_qty, min_stock, max_stock, item_prio_, storage_cost, mhd)
VALUES
(
97006510, 'Meat', 'KG ', 68, 340, 25, 120, 1, 0.004208, 3
);

